package com.arqui.jobs.presentation.main.jobs;

import android.content.Context;


import com.arqui.jobs.data.entities.JobEntity;
import com.arqui.jobs.data.entities.trackholder.TrackEntityHolder;
import com.arqui.jobs.data.local.SessionManager;
import com.arqui.jobs.data.remote.ServiceFactory;
import com.arqui.jobs.data.remote.request.ListRequest;
import com.arqui.jobs.data.remote.request.PostRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 31/05/17.
 */

public class JobsPresenter implements JobsContract.Presenter, JobItem {

    private final JobsContract.View mView;
    private final SessionManager mSessionManager;
    private Context context;
    private boolean firstLoad = false;
    private int currentPage = 1;

    public JobsPresenter(JobsContract.View mView, Context context) {
        this.mView = mView;
        this.mSessionManager = new SessionManager(context);
        this.mView.setPresenter(this);

    }
    @Override
    public void clickItem(JobEntity jobEntity) {
        mView.clickItemJob(jobEntity);

    }

    @Override
    public void deleteItem(JobEntity jobEntity, int position) {

    }


    @Override
    public void start() {
    }

    @Override
    public void loadListJobs(int id) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<TrackEntityHolder<JobEntity>> reservation = listRequest.getAdminJobs(id);
        reservation.enqueue(new Callback<TrackEntityHolder<JobEntity>>() {
            @Override
            public void onResponse(Call<TrackEntityHolder<JobEntity>> call, Response<TrackEntityHolder<JobEntity>> response) {
                mView.setLoadingIndicator(false);
                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {

                    mView.getJobsList(response.body().getJobs());

                } else {
                    mView.showErrorMessage("Error al obtener la lista");
                }
            }

            @Override
            public void onFailure(Call<TrackEntityHolder<JobEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void loadList() {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<TrackEntityHolder<JobEntity>> reservation = listRequest.getJobs();
        reservation.enqueue(new Callback<TrackEntityHolder<JobEntity>>() {
            @Override
            public void onResponse(Call<TrackEntityHolder<JobEntity>> call, Response<TrackEntityHolder<JobEntity>> response) {
                mView.setLoadingIndicator(false);
                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {

                    mView.getJobsList(response.body().getJobs());

                } else {
                    mView.showErrorMessage("Error al obtener la lista");
                }
            }

            @Override
            public void onFailure(Call<TrackEntityHolder<JobEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void sendDatoForJob(int idJob, int idUser) {
         mView.setLoadingIndicator(true);
        PostRequest postRequest = ServiceFactory.createService(PostRequest.class);
        final Call<Void> reservation = postRequest.sendMyAplication(idUser, idJob);
        reservation.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                mView.setLoadingIndicator(false);
                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {

                    if(response.code() == 201){
                        mView.responseSendData("Se ha asignado correctamente");
                    }else{
                        mView.showErrorMessage("Error al obtener la lista");
                    }

                } else {
                    mView.showErrorMessage("Error al obtener la lista");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
