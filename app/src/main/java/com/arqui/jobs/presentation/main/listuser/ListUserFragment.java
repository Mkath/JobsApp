package com.arqui.jobs.presentation.main.listuser;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arqui.jobs.R;
import com.arqui.jobs.core.BaseActivity;
import com.arqui.jobs.core.BaseFragment;
import com.arqui.jobs.core.RecyclerViewScrollListener;
import com.arqui.jobs.core.ScrollChildSwipeRefreshLayout;
import com.arqui.jobs.data.entities.UserByJob;
import com.arqui.jobs.data.entities.UserEntity;
import com.arqui.jobs.presentation.profile.ProfileActivity;
import com.arqui.jobs.utils.ProgressDialogCustom;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by katherine on 31/05/17.
 */

public class ListUserFragment extends BaseFragment implements UserContract.View {


    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.noListIcon)
    ImageView noListIcon;
    @BindView(R.id.noListMain)
    TextView noListMain;
    @BindView(R.id.noList)
    LinearLayout noList;
    @BindView(R.id.refresh_layout)
    ScrollChildSwipeRefreshLayout refreshLayout;
    Unbinder unbinder;
    @BindView(R.id.profile)
    FloatingActionButton profile;

    private UserAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private UserContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;

    private int idJob;

    public ListUserFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadListUsers(idJob);

    }

    public static ListUserFragment newInstance(Bundle bundle) {
        ListUserFragment fragment = new ListUserFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idJob = getArguments().getInt("idJob");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        final ScrollChildSwipeRefreshLayout swipeRefreshLayout =
                (ScrollChildSwipeRefreshLayout) root.findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.black),
                ContextCompat.getColor(getActivity(), R.color.dark_gray),
                ContextCompat.getColor(getActivity(), R.color.black)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        swipeRefreshLayout.setScrollUpChild(rvList);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //mPresenter.start();
                mPresenter.loadListUsers(idJob);
            }
        });

        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Obteniendo datos...");
        mLayoutManager = new LinearLayoutManager(getContext());
        rvList.setLayoutManager(mLayoutManager);
        mAdapter = new UserAdapter(new ArrayList<UserEntity>(), getContext(), (UserItem) mPresenter);
        rvList.setAdapter(mAdapter);
    }

    @Override
    public void getUsersList(ArrayList<UserByJob> list) {
        ArrayList<UserEntity> userEntities = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            UserEntity userEntity = new UserEntity(
                    null, list.get(i).getUserLastName1(), list.get(i).getUserName(),
                    null, null, 0);
            userEntities.add(userEntity);
        }

        mAdapter.setItems(userEntities);
        if (list != null) {
            noList.setVisibility((list.size() > 0) ? View.GONE : View.VISIBLE);
        }

        rvList.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {
                //mPresenter.loadListUsers(idJob);
            }
        });
    }

    @Override
    public void clickItemUsers(UserEntity userEntity) {

        Bundle bundle = new Bundle();
        bundle.putSerializable("userEntity", userEntity);
        nextActivity(getActivity(), bundle, ProfileActivity.class, false);

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(UserContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl =
                (SwipeRefreshLayout) getView().findViewById(R.id.refresh_layout);

        // Make sure setRefreshing() is called after the layout is done with everything else.
        srl.post(new Runnable() {
            @Override
            public void run() {
                srl.setRefreshing(active);
            }
        });

        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @OnClick(R.id.profile)
    public void onViewClicked() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("myProfile", true);
        nextActivity(getActivity(), bundle, ProfileActivity.class, false);
    }
}
