package com.arqui.jobs.presentation.main.listuser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arqui.jobs.R;
import com.arqui.jobs.core.LoaderAdapter;
import com.arqui.jobs.data.entities.JobEntity;
import com.arqui.jobs.data.entities.UserEntity;
import com.arqui.jobs.utils.OnClickListListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by katherine on 31/05/17.
 */

public class UserAdapter extends LoaderAdapter<UserEntity> implements OnClickListListener {

    private Context context;
    private UserItem userItem;

    public UserAdapter(ArrayList<UserEntity> userEntities, Context context, UserItem userItem) {
        super(context);
        setItems(userEntities);
        this.context = context;
        this.userItem = userItem;
    }

    public UserAdapter(ArrayList<UserEntity> userEntities, Context context) {
        super(context);
        setItems(userEntities);
        this.context = context;

    }

    public ArrayList<UserEntity> getItems() {
        return (ArrayList<UserEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_users, parent, false);
        return new ViewHolder(root, this);
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserEntity userEntity = getItems().get(position);
        //((ViewHolder) holder).cardView.setBackgroundResource(ticketEntity.getSchedules().getDestiny().getImage_1());
        ((ViewHolder) holder).tvName.setText(userEntity.getFullName());
        ((ViewHolder) holder).tvEmail.setText(userEntity.getUserEmail());
        ((ViewHolder) holder).tvPhone.setText(userEntity.getUserPhone());

    }

    @Override
    public void onClick(int position) {
        UserEntity userEntity = getItems().get(position);
        userItem.clickItem(userEntity);
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_email)
        TextView tvEmail;
        @BindView(R.id.tv_phone)
        TextView tvPhone;
        private OnClickListListener onClickListListener;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
