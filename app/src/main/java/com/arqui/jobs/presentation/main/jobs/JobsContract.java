package com.arqui.jobs.presentation.main.jobs;


import com.arqui.jobs.core.BasePresenter;
import com.arqui.jobs.core.BaseView;
import com.arqui.jobs.data.entities.JobEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 31/05/17.
 */

public interface JobsContract {
    interface View extends BaseView<Presenter> {

        void getJobsList(ArrayList<JobEntity> list);

        void clickItemJob(JobEntity jobEntity);

        void responseSendData(String message);

        boolean isActive();



    }

    interface Presenter extends BasePresenter {

        void loadListJobs(int id);

        void loadList();

        void sendDatoForJob(int idJob, int idUser);

    }
}
