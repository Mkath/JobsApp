package com.arqui.jobs.presentation.register;


import android.support.annotation.NonNull;

import com.arqui.jobs.core.BasePresenter;
import com.arqui.jobs.core.BaseView;
import com.arqui.jobs.data.entities.UserEntity;

/**
 * Created by katherine on 3/05/17.
 */

public interface RegisterContract {
    interface View extends BaseView<Presenter> {

        void registerSuccessful(UserEntity userEntity);
        void errorRegister(String msg);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        void registerUser(@NonNull UserEntity userEntity);

    }
}
