package com.arqui.jobs.presentation.main.listuser;

import android.content.Context;

import com.arqui.jobs.data.entities.JobEntity;
import com.arqui.jobs.data.entities.UserByJob;
import com.arqui.jobs.data.entities.UserEntity;
import com.arqui.jobs.data.entities.trackholder.TrackEntityHolderUser;
import com.arqui.jobs.data.local.SessionManager;
import com.arqui.jobs.data.remote.ServiceFactory;
import com.arqui.jobs.data.remote.request.ListRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 31/05/17.
 */

public class UserPresenter implements UserContract.Presenter, UserItem {

    private final UserContract.View mView;
    private final SessionManager mSessionManager;
    private Context context;
    private boolean firstLoad = false;
    private int currentPage = 1;

    public UserPresenter(UserContract.View mView, Context context) {
        this.mView = mView;
        this.mSessionManager = new SessionManager(context);
        this.mView.setPresenter(this);

    }



    @Override
    public void start() {
    }

    @Override
    public void clickItem(UserEntity userEntity) {
        mView.clickItemUsers(userEntity);

    }

    @Override
    public void loadListUsers(int id) {
    mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<TrackEntityHolderUser<UserByJob>> reservation = listRequest.getUsersByJobs(id);
        reservation.enqueue(new Callback<TrackEntityHolderUser<UserByJob>>() {
            @Override
            public void onResponse(Call<TrackEntityHolderUser<UserByJob>> call, Response<TrackEntityHolderUser<UserByJob>> response) {
                mView.setLoadingIndicator(false);
                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    if(response.code() == 200){
                        mView.getUsersList(response.body().getUsers());
                    }

                } else {
                    mView.showErrorMessage("Error al obtener la lista");
                }
            }

            @Override
            public void onFailure(Call<TrackEntityHolderUser<UserByJob>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
