package com.arqui.jobs.presentation.auth;

import com.arqui.jobs.core.BasePresenter;
import com.arqui.jobs.core.BaseView;
import com.arqui.jobs.data.entities.AccessTokenEntity;
import com.arqui.jobs.data.entities.UserEntity;

/**
 * Created by katherine on 12/05/17.
 */

public interface LoginContract {
    interface View extends BaseView<Presenter> {
        void loginSuccessful(UserEntity userEntity);
        void errorLogin(String msg);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        void loginUser(String username, String password, int userType);
        void openSession(AccessTokenEntity token, UserEntity userEntity);
    }
}
