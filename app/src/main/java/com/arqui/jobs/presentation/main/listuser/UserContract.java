package com.arqui.jobs.presentation.main.listuser;


import com.arqui.jobs.core.BasePresenter;
import com.arqui.jobs.core.BaseView;
import com.arqui.jobs.data.entities.JobEntity;
import com.arqui.jobs.data.entities.UserByJob;
import com.arqui.jobs.data.entities.UserEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 31/05/17.
 */

public interface UserContract {
    interface View extends BaseView<Presenter> {

        void getUsersList(ArrayList<UserByJob> list);

        void clickItemUsers(UserEntity userEntity);

        boolean isActive();



    }

    interface Presenter extends BasePresenter {

        void loadListUsers(int id);

    }
}
