package com.arqui.jobs.data.remote.request;

import com.arqui.jobs.data.entities.AccessTokenEntity;
import com.arqui.jobs.data.entities.UserEntity;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by katherine on 10/05/17.
 */

public interface RegisterRequest {

    @FormUrlEncoded
    @POST("users/register")
    Call<AccessTokenEntity> registerUser(@Field("userEmail") String email,
                                         @Field("userName") String first_name,
                                         @Field("userLastName1") String last_name,
                                         @Field("userPassword") String password,
                                         @Field("userPhone") String phone,
                                         @Field("userType") int userType);


}

