package com.arqui.jobs.data.remote.request;

import com.arqui.jobs.data.entities.UserByJob;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by kath on 25/01/18.
 */

public interface PostRequest {

    @FormUrlEncoded
    @POST("users/aplicationoffer")
    Call<Void> sendMyAplication(@Field("idUser") int idUser,
                                     @Field("idJob") int idJob);

}
